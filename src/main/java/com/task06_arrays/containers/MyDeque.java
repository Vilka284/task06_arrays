package com.task06_arrays.containers;

import java.util.ArrayList;

public class MyDeque<E> {
    private ArrayList<E> al;

    public MyDeque() {
        this.al = new ArrayList<>();
    }

    public void addFirst(E element) {
        al.add(element);
        for (int i = 0; i < al.size() - 1; i++) {
            al.set(al.size() - 1 - i, al.get(al.size() - 2 - i));
        }
        al.set(0, element);
    }

    public E removeFirst() {
        if(al.isEmpty()){
            return null;
        }
        E removeElement = al.get(0);
        al.remove(0);
        return removeElement;
    }

    public E getFirst() {
        return al.get(0);
    }

    public void addLast(E element) {
        al.add(element);
    }

    public E removeLast() {
        if(al.isEmpty()){
            return null;
        }
        E removeElement = al.get(al.size() - 1);
        al.remove(al.size() - 1);
        return removeElement;
    }

    public E getLast(){
        return al.get(al.size() - 1);
    }

    @Override
    public String toString() {
        String result="[ ";

        for (E element:al) {
            result +=element+", ";
        }
        result +="]";
        return result;
    }
}
