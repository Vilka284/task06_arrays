package com.task06_arrays.containers;

public class MyStringContainer {
    private String[] str;

    MyStringContainer(String[] str){
        this.str = str;
    }

    public String[] getStr() {
        return str;
    }

    public void addStr(String str) {
        String[] str1 = new String[this.str.length+1];
        str1[this.str.length] = str;
        this.str = str1;
    }
}
