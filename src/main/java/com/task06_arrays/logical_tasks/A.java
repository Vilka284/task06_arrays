package com.task06_arrays.logical_tasks;

import java.util.Arrays;
import java.util.Scanner;

public class A {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String[] s = in.nextLine().split(" ");
        int[] a = new int[s.length];
        for (int i = 0; i < s.length; i++)
            a[i] = Integer.parseInt(s[i]);


        String[] m = in.nextLine().split(" ");
        int[] b = new int[m.length];
        for (int i = 0; i < m.length; i++)
            b[i] = Integer.parseInt(m[i]);

        // a
        int[] c = new int[Math.max(a.length, b.length)];
        int n = 0;
        for (int i = 0; i < a.length; i++){
            for (int j = 0; j < b.length; j++){
                if (a[i] == b[j]){
                    c[n] = a[i];
                    n++;
                    break;
                }
            }
        }
        System.out.println(Arrays.toString(c));

        //b
        c = new int[Math.max(a.length, b.length)];
        n = 0;
        for (int i = 0; i < a.length; i++){
                if (Arrays.binarySearch(b, a[i]) == -1){
                    c[n] = a[i];
                    n++;
                }
        }
        System.out.println(Arrays.toString(c));
    }
}
