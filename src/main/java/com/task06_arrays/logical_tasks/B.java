package com.task06_arrays.logical_tasks;


import java.util.Arrays;
import java.util.Scanner;

public class B {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        String[] s = in.nextLine().split(" ");
        int[] a = new int[s.length];
        int n = s.length;
        for (int i = 0; i < s.length; i++)
            a[i] = Integer.parseInt(s[i]);

        Arrays.sort(a);
        int[] temp = new int[n];

        int j = 0;
        for (int i=0; i<n-1; i++)
            if (a[i] != a[i+1])
                temp[j++] = a[i];


        temp[j++] = a[n-1];

        if (j >= 0) System.arraycopy(temp, 0, a, 0, j);

        for (int i = 0; i < j; i++)
            System.out.print(a[i] + ", ");
    }
}
