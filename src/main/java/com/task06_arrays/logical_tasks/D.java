package com.task06_arrays.logical_tasks;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class D {

    private static Random random = new Random();
    private static Scanner in = new Scanner(System.in);

    private static class Hero {
        int strength;

        public Hero() {
            this.strength = 25;
        }

        public int getStrength() {
            return strength;
        }

        public void setStrength(int strength) {
            this.strength = strength;
        }
    }

    private interface Playable {

        int getStrength();

        String getType();
    }

    private static class Monster implements Playable {
        int strength;
        String className;

        public Monster() {
            this.strength = 5 + random.nextInt(95);
            className = this.getClass().getSimpleName();
        }

        @Override
        public int getStrength() {
            return this.strength;
        }

        @Override
        public String getType() {
            return this.className;
        }
    }

    private static class Artifact implements Playable {
        int strength;
        String className;

        public Artifact() {
            this.strength = 10 + random.nextInt(70);
            className = this.getClass().getSimpleName();
        }

        @Override
        public int getStrength() {
            return this.strength;
        }

        @Override
        public String getType() {
            return this.className;
        }
    }

    public static void main(String[] args) {
        Hero hero;
        ArrayList<Playable> doors;
        System.out.println("Hello there!\n"+
                "You play as a Hero against monsters\n"+
                "I will help you to defeat them");


        String choice = "y";
        while (choice.equals("y")) {
            hero = new Hero();
            doors = fillDoors();
            printGame(doors, hero);
            System.out.println("Wanna try again? (y/n)");
            in.nextLine();
            choice = in.nextLine();
        }


        System.out.println("Bye!");
    }

    private static void printGame(ArrayList<Playable> doors, Hero hero){
        boolean choice = true;
        while (choice) {
            printHeroStats(hero);
            printMenu(doors, hero);
            System.out.println("Choose door you want to enter");
            int n;
            try{
                n = in.nextInt();
            }catch (RuntimeException er){
                System.out.println("Input correct number!");
                n = -1;
            }

            try{
                doors.get(n);
            }catch (RuntimeException e){
                System.out.println("Wrong number! Try again");
                n = -1;
            }

            if (n == -1){
                continue;
            }

            if (doors.get(n).getType().equals("Monster")) {
                hero.setStrength(hero.getStrength() - doors.get(n).getStrength());
                doors.remove(doors.get(n));
            } else if (doors.get(n).getType().equals("Artifact")) {
                hero.setStrength(hero.getStrength() + doors.get(n).getStrength());
                doors.remove(doors.get(n));
            }

            if (hero.getStrength() < 0) {
                printEnd();
                System.out.println("            You died!");
                choice = false;
            }

            if (doors.isEmpty()){
                printEnd();
                System.out.println("            You win!");
                choice = false;
            }
        }
    }

    private static void printMenu(ArrayList<Playable> doors, Hero hero){
        int size = doors.size();
        //types print
        for (int i = 0; i < size; i++) {
            System.out.print(doors.get(i).getType() + " ");
        }
        System.out.print("\n");

        //doors print
        for (int i = 0; i < size; i++) {
            System.out.print("___" + "  ");
        }
        System.out.print("\n");

        //strength print
        for (int i = 0; i < size; i++) {
            System.out.print("|" + doors.get(i).getStrength() + "|" + " ");
        }
        System.out.print("\n");

        //complexity print
        for (int i = 0; i < size; i++) {
            String n;
            if (hero.getStrength() > doors.get(i).getStrength()) {
                n = "✓";
            } else {
                n = "✗";
            }
            if (doors.get(i).getType().equals("Artifact")){
                n = "A";
            }
            System.out.print("|" + n +" |" + " ");
        }
        System.out.print("\n");

        for (int i = 0; i < size; i++) {
            System.out.print("|_ |" + " ");
        }
        System.out.print("\n");

        //print numbers
        for (int i = 0; i < size; i++) {
            System.out.print("  " + i + "  ");
        }
        System.out.print("\n");
    }

    private static void printHeroStats(Hero hero){
        System.out.println("___________________________");
        System.out.println("| Strength: " + hero.getStrength() + "           |");
        System.out.println("___________________________");
    }

    private static void printEnd(){
        for (int i = 1; i < 40; i++) {
            for (int j = 1; j < 40; j++) {
                if (i % 3 == 0 || j % 3 == 0){
                    System.out.print("x");
                }else{
                    System.out.print(" ");
                }
            }
            System.out.print("\n");
        }
    }

    private static ArrayList<Playable> fillDoors(){
        ArrayList<Playable> doors = new ArrayList<>(10);
        for (int i = 0; i < 10; i++) {
            Playable p;
            int n = random.nextInt(2);
            if (n == 1) {
                p = new Monster();
                doors.add(p);
            } else {
                p = new Artifact();
                doors.add(p);
            }
        }
        return doors;
    }
}
